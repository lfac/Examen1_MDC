#Luisa Fernanda Aguilar
#examen 1
#inciso G

#!/usr/bin/env Rscript

require(matrixStats)
require(ggplot2)

revenue <- 10000 * accepted_bin * (1 - default_bin) * array_amount_good * array_total_interest
default_cost <- 10000 * accepted_bin * default_bin * array_amount_bad * array_default
marketing    <- 1000000
utilities    <- revenue - default_cost 

plot(score_cuts, utilities, type = 'l')

